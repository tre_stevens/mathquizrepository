package com.example.mathquiz;

import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button b_start, b_answer0, b_answer1, b_answer2, b_answer3;
    TextView tv_score, tv_timer, tv_questions, tv_bottomMessage;
    ProgressBar prog_timer;

    Game g = new Game();
    int secondsRemaining = 30;

    CountDownTimer timer = new CountDownTimer(30000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            secondsRemaining --;
            tv_timer.setText(Integer.toString(secondsRemaining) + "sec");
            prog_timer.setProgress(30 - secondsRemaining);
        }

        @Override
        public void onFinish() {
            b_answer0.setEnabled(false);
            b_answer1.setEnabled(false);
            b_answer2.setEnabled(false);
            b_answer3.setEnabled(false);
            tv_bottomMessage.setText("Time is up! " + g.getNumberCorrect() + "/" + (g.getTotalQuestions() -1));

            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    b_start.setVisibility(View.VISIBLE);
                }
            }, 4000);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_start = findViewById(R.id.b_start);
        b_answer0 = findViewById(R.id.b_answer0);
        b_answer1 = findViewById(R.id.b_answer1);
        b_answer2 = findViewById(R.id.b_answer2);
        b_answer3 = findViewById(R.id.b_answer3);

        tv_score = findViewById(R.id.tv_score);
        tv_timer = findViewById(R.id.tv_timer);
        tv_questions = findViewById(R.id.tv_questions);
        tv_bottomMessage = findViewById(R.id.tv_bottomMessage);

        tv_timer.setText("0sec");
        tv_questions.setText("");
        tv_bottomMessage.setText("Press Go!");
        tv_score.setText("0pts");

        prog_timer = findViewById(R.id.prog_timer);

        View.OnClickListener answerButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button buttonClicked = (Button) v;

                int answerSelected = Integer.parseInt(buttonClicked.getText().toString());

                g.checkAnswer(answerSelected);
                tv_score.setText(Integer.toString(g.getScore())+ "pts");
                nextTurn();

            }
        };

        View.OnClickListener startButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button start_button = (Button) v;

                start_button.setVisibility(View.INVISIBLE);
                secondsRemaining = 30;
                tv_score.setText("0 pts");
                g = new Game();
                nextTurn();
                timer.start();

            }
        };

        b_start.setOnClickListener(startButtonClickListener);

        b_answer0.setOnClickListener(answerButtonClickListener);
        b_answer1.setOnClickListener(answerButtonClickListener);
        b_answer2.setOnClickListener(answerButtonClickListener);
        b_answer3.setOnClickListener(answerButtonClickListener);

    }

    private void nextTurn(){
        // Create a new question
        g.makeNewQuestion();
        // Set text on answer button
       int[] answer = g.getCurrentQuestion().getAnswerArray();
        b_answer0.setText(Integer.toString(answer[0]));
        b_answer1.setText(Integer.toString(answer[1]));
        b_answer2.setText(Integer.toString(answer[2]));
        b_answer3.setText(Integer.toString(answer[3]));

        b_answer0.setEnabled(true);
        b_answer1.setEnabled(true);
        b_answer2.setEnabled(true);
        b_answer3.setEnabled(true);

        tv_questions.setText(g.getCurrentQuestion().getQuestionPhrase());

        tv_bottomMessage.setText(g.getNumberCorrect() + "/" + (g.getTotalQuestions() - 1));

        // Enable answer buttons
        // Start timer

    }
}
